package csc403finalProject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


public class Classifiers {
	//Ideas for this class came from http://shiffman.net/teaching/a2z/bayesian/
	private ArrayList<String> word;//for each word
	private ArrayList<Integer> timesUsed;//count the number of times the word is used

	public Classifiers(String word){
		//creating a word and initialize all variables to 0
		this.word = new ArrayList<String>();
		this.timesUsed = new ArrayList<Integer>();
		this.timesUsed.add(1);
		this.word.add(word);
	}

	private static void proof(HashMap<String, Classifiers> map){
		//This is proof that my Class does count the links. also it was very helpful for finding errors.
		for (String key: map.keySet()) {
			System.out.println("\nthe Key is: "+key );
			Classifiers values = map.get(key);
			for(int index =0; index < values.word.size(); index++){
				System.out.println(" The value is: " +values.word.get(index)+" The link count is: "
						+ values.timesUsed.get(index));
			}
		}

	}

	private static <Word> String mumble(HashMap<String, Classifiers> words, ArrayList<String> originalArray, int sentanceLen){
		/*This method take the hashmap, the originalArray and the length that the user wants the sentance to be and returns a 
		 * a sentance of sentanceLen or less, using the hashmap
		 */

		Random generator = new Random();
		//to create random numbers
		int hashMaplen = originalArray.size();
		//giving me the len of the array for random numbers
		int randomNum = generator.nextInt(hashMaplen-1);
		//finally have my random number
		String startKey = originalArray.get(randomNum);
		//getting the start key I use the array and and index it to when the random number is.
		Classifiers nextWord = new Classifiers(startKey);
		//the values of the hashmap are of the classifiers type I have changed the start value to that
		int classifierWordLen =nextWord.word.size();
		int randomIndex = generator.nextInt(classifierWordLen);
		String printedWord =nextWord.word.get(randomIndex); 
		int sentanceCounter = 0;
		String sentance = "";
		/*this while loop continues until we have either found the last word in the orginal input or
		 * it will continue till we have printed words of sentanceLen
		 * in the if statement we print the word and get the next word from the hashmap
		 * since the value words in the hashmap are in an arraylist we randomly choose the next word with the
		 * randomIndex */
		while(sentanceCounter < sentanceLen){
			if(words.containsKey(printedWord)){
				sentance += printedWord+" ";
				nextWord = words.get(printedWord);
				classifierWordLen =nextWord.word.size();
				randomIndex = generator.nextInt(classifierWordLen);
				printedWord =nextWord.word.get(randomIndex); 
				sentanceCounter++;

			}
			else{
				//If I got the last word then I choose a new word from the original array
				//and keep going.
				sentance+= printedWord+" ";
				nextWord = words.get(originalArray.get(randomNum));
				classifierWordLen = nextWord.word.size();
				randomIndex = generator.nextInt(classifierWordLen);
				printedWord =nextWord.word.get(randomIndex); 
				sentanceCounter++;

			}
		}
		return sentance;

	}

	private static HashMap<String, Classifiers> learn(ArrayList<String> wordsList){
		//This method takes an array of strings and returns a hash map the key will
		//be a string and the value will be of type classifiers
		HashMap<String, Classifiers> words = new HashMap<String, Classifiers>();	
		int length = wordsList.size();
		for(int i=0; i < length-1; i++){
			String word = wordsList.get(i);
			String nextWord = wordsList.get(i+1);
			Classifiers values = words.get(word);
			if(words.containsKey(word)){
				if(values.word.contains(nextWord)){
					//if we already have the word and value we increment the link count
					Classifiers newWord = words.get(word);
					int index = newWord.word.indexOf(nextWord);
					newWord.timesUsed.set(index,newWord.timesUsed.get(index)+1);
				}
				else{
					//we are adding a new word to the arrayList in classifiers
					Classifiers newWord = words.get(word);
					newWord.word.add(nextWord);
					newWord.timesUsed.add(1);
					words.put(word, newWord);
				}

			}
			else{
				//if this is the first time we have seen the key we put it in the array list.
				Classifiers newWord = new Classifiers(nextWord);
				newWord.timesUsed.add(1);
				words.put(word, newWord);

			}
		}

		return words;
	}
	public static void main(String[] args) throws IOException {
		//prompt to get user input
		System.out.println("Please enter a file.");
		//reads the file name
		Scanner scan = new Scanner(System.in);
		String filename = scan.next();
		//ArrayList that will have each word from the text
		ArrayList<String> wordsList= new ArrayList<String>();
		//opens the file and puts each word in an ArrayList
		//Idea taken from http://stackoverflow.com/questions/4574041/read-next-word-in-java
		  Scanner sc2 = null;
		    try {
		        sc2 = new Scanner(new File(filename));
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();  
		    }
		    while (sc2.hasNextLine()) {
		            Scanner s2 = new Scanner(sc2.nextLine());
		        boolean b;
		        while (b = s2.hasNext()) {
		            String s = s2.next();
		            wordsList.add(s);
		        }
		    }
		    //gets an integer from user to determine the length of the string.
		    Scanner userInput = new Scanner(System.in);
		    int userInt;
			do{
				System.out.println("Please enter an Integer");
				userInt = userInput.nextInt();	
			}
			while(userInt >= wordsList.size());
		//creates a hashmap
		HashMap<String, Classifiers> hashWords = learn(wordsList);
		//gives us a random sentance
		String jubble = mumble(hashWords, wordsList, userInt);
		System.out.print(jubble);
		//proof is a debugging method I made and I am leaving in this class just in case 
		//anyone needs to see that the class is working as it was intended to.
		//Especially if you want to see that the links are being counted.
		//proof(hashWords);
	}

}
